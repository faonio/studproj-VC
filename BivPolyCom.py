from charm.toolbox.pairinggroup import ZR, G1, G2, pair
import random as rd

class Biv: 

    def __init__(self, group, d, l):
        self.group = group
        self.d = d
        self.l = l

    def ComGen(self, d, l):
        """Generates the commitment key
        Args:
            d (int): degree in X
            l (int): degree in Y

        Returns:
            dictionary: commitment key
        """
        g, h = self.group.random(G1), self.group.random(G1)
        g_gothic = self.group.random(G2)
        alpha, s, t = self.group.random(ZR), self.group.random(ZR), self.group.random(ZR)

        g_i_j = []
        g_hat_i_j = []
        for i in range (self.d):
            g_i_j.append([])
            g_hat_i_j.append([])
            for j in range (self.l):
                g_i_j[i].append(g**((s**i)*(t**j)))
                g_hat_i_j[i].append((g**alpha)**((s**i)*(t**j)))

        # Those variables are useful for SNARK
        g_gothic1 = g_gothic**s
        h1=h**s 

        return {'g_i_j' : g_i_j, 'g_hat_i_j' : g_hat_i_j, 'h' : h, 'h_hat' : h**alpha, 'g_gothic' : g_gothic, 'g_gothic_hat' : g_gothic**alpha, 'g_gothic1' : g_gothic1, 'h1' : h1}


    def Com(self, ck, P):
        """Creates of the commitment for the bivariate polynomial P

        Args:
            ck (dictionary): commitment key
            P (matrix): committed bivariate polynomial

        Returns:
            dictionary: two uniformly distributed elements of G1²
            ZR element: random ZR element
        """
        rho = self.group.random(ZR)
        product1 = 1
        product2 = 1
        for i in range(len(P)):
            for j in range(len(P[0])):
                product1 = product1 * (ck['g_i_j'][i][j]**(P[i][j]))
                product2 = product2 * (ck['g_hat_i_j'][i][j]**(P[i][j]))
        c = (ck['h']**rho) * product1
        c_hat = (ck['h_hat']**rho) * product2
        C = {'c' : c, 'c_hat' : c_hat}
        return C, rho

    def ComVer(self, ck, C):
        """Verifies the commitment

        Args:
            ck (dictionary): commitment key
            C (dictionary): values created from the committed polynomal

        Returns:
            boolean: verification of the commitment
        """
        c = C['c']
        c_hat = C['c_hat']
        g_gothic = ck['g_gothic']
        g_gothic_hat = ck['g_gothic_hat']
        b = (pair(c, g_gothic_hat) == pair(c_hat, g_gothic))
        return b


    def OpenVer(self, ck, C, a, rho):
        """Global verification

        Args:
            ck (dictionary): commitment key
            C (dictionary): values computed from the committed polynomial
            a (matrix): committed bivariate polynomial
            rho (ZR element): random value used to compute C

        Returns:
            boolean: validation
        """
        # Committment verification
        b1 = self.ComVer(ck, C)

        # Verification of the value of c
        product = 1
        for i in range(self.d):
            for j in range(self.l):
                product = product * (ck['g_i_j'][i][j]**(a[i][j]))

        b2 = (C['c'] ==(ck['h']**rho)*product)

        # Thanks to the BPKE Assumption, if b1 and b2 are true, then it is the right polynomial
        return (b1 and b2)