from charm.toolbox.pairinggroup import ZR, pair
import polynomial


class BivPE:

    def __init__(self, group, biv, d, l):
        self.group = group
        self.q = group.order()
        self.biv = biv
        self.d = d
        self.l = l


    def Gen(self, ck, P, Q, k):
        """Generates of all needed variables for the CaP

        Args:
            ck (dictionnary): commitment key given by Biv.ComGen
            P (matrix): bivariate polynomial
            Q (matrix): P(k, Y)
            k (int): evalutation point (random value)

        Returns:
            dictionnary: variables needeed to prove and verify statments for the CaP relation (crs)
            dictionnary: statement (u)
            dictionnary: witness (w)
        """

        crs = ck

        # Commitments for P and Q
        C, rho = self.biv.Com(ck, P)
        C_prime, rho_prime = self.biv.Com(crs, Q)

        # Statement
        u = {'C' : C, 'C_prime' : C_prime, 'k' : k}
        # Witness
        w = {'P' : P, 'Q' : Q, 'rho' : rho, 'rho_prime' : rho_prime}

        return crs, u, w


    def Prove(self, crs, u, w):
        """Prover: generates the proof that the CaP relation is satisfied

        Args:
            crs (dictionnary): variables needeed to prove and verify statments for the CaP relation
            u (dictionnary): statement
            w (dictionnary): witness 

        Returns:
            dictionnary: proof that the CaP relation is satisfied
        """

        C, C_prime, k = u['C'], u['C_prime'], u['k']
        P, Q, rho, rho_prime = w['P'], w['Q'], w['rho'], w['rho_prime']
        h, h1, g_gothic = crs['h'], crs['h1'], crs['g_gothic']

        #P_prime = P - Q
        P_prime = []
        for i in range(self.d):
            P_prime.append([])
            for j in range (self.l):
                P_prime[i].append(P[i][j] - Q[i][j])

        #R = X - k
        R = [[-k], [1]] 

        # W = (P - Q) / (X - k) = P_prime / R
        W = polynomial.division(P_prime, R, self.q)

        # W commitment
        D, omega = self.biv.Com(crs, W)


        # Schnorr proof protocol
        g_tilde = h1 / (h ** k)
        x, y = self.group.random(ZR), self.group.random(ZR)
        U = pair(((h**x)*(g_tilde**y)), g_gothic)
        e = self.group.hash((u, D, U))
        sigma = (x - (rho_prime - rho)*e)
        tau = (y - omega*e) 
        
        # Proof to send to the verifier
        pi = {'D' : D, 'e' : e, 'sigma' : sigma, 'tau' : tau}
        return pi


    def Ver(self, crs, u, pi):
        """Verifier: verifies the proof pi

        Args:
            crs (dictionnary): variables needeed to prove and verify statments for the CaP relation
            u (dictionnary): statement
            pi (dictionnary): proof we have to verify

        Returns:
            boolean: 'True' if the proof is verified
        """

        # Parsing the received values
        C, C_prime, k = u['C'], u['C_prime'], u['k']
        D, e, sigma, tau = pi['D'], pi['e'], pi['sigma'], pi['tau']
        c, c_hat = C['c'], C['c_hat']
        c_prime, c_prime_hat = C_prime['c'], C_prime['c_hat']
        d, d_hat = D['c'], D['c_hat']
        h, h1, g_gothic, g_gothic1 = crs['h'], crs['h1'], crs['g_gothic'], crs['g_gothic1']
        
        # Verifing the received commitments
        b1 = self.biv.ComVer(crs, C)        
        b2 = self.biv.ComVer(crs, C_prime)
        b3 = self.biv.ComVer(crs, D)

        # Verification procedure of the Schnorr proof protocol
        A = pair(d, (g_gothic1 / (g_gothic** k))) / (pair(c/c_prime, g_gothic))
        g_tilde = h1 / (h**k)
        U = pair((h**sigma)*(g_tilde**tau), g_gothic)*(A**e)        
        b4 = (e == self.group.hash((u, D, U)))

        # If all the commitments and the Schnorr proof are verified, then the proof is verified
        b = b1 and b2 and b3 and b4

        return b
