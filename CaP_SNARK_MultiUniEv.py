from CaP_SNARK_BivPE import BivPE


class MultiUniPE:

    def __init__(self, group, biv, d, l):
        self.group = group
        self.q = group.order()
        self.biv = biv
        self.d = d
        self.l = l


    def Gen(self, ck, P, Q, k):
        """Generates of all needed variables for the CaP

        Args:
            ck (dictionnary): commitment key given by MPoly.ComGen
            P (matrix): list of univariate polynomials
            Q (matrix): P(k, Y)
            k (int): evalutation point (random value)

        Returns:
            dictionnary: variables needeed to prove and verify statments for the CaP relation (crs)
            dictionnary: statement (u)
            dictionnary: witness (w)
        """

        crs, u, w = BivPE.Gen(self, ck, P, Q, k)

        return crs, u, w


    def Prove(self, crs, u, w):
        """Prover: generates the proof that the CaP relation is satisfied

        Args:
            crs (dictionnary): variables needeed to prove and verify statments for the CaP relation
            u (dictionnary): statement
            w (dictionnary): witness 

        Returns:
            dictionnary: proof that the CaP relation is satisfied
        """

        pi = BivPE.Prove(self, crs, u, w)

        return pi

    def Ver(self, crs, u, pi):
        """Verifier: verifies the proof pi

        Args:
            crs (dictionnary): variables needeed to prove and verify statments for the CaP relation
            u (dictionnary): statement
            pi (dictionnary): proof we have to verify

        Returns:
            boolean: 'True' if the proof is verified
        """
        
        b = BivPE.Ver(self, crs, u, pi)
        return b

    