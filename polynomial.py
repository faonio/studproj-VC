from scipy.fft import fft2, ifft2 

def extend_polynomial(p, d, l):
    """Extends a bivariate polynomial p to be of degrees d in X and l in Y

    Args:
        p (matrix): bivariate polynomial to be extended
        d (int): wanted degree in X
        l (int): wanted degree in Y

    Returns:
        matrix: bivariate polynomial of degrees d in X and l in Y
    """
    P_extended = []
    for i in range (d):
        if(i<len(p)):
            P_extended.append( list(p[i]) + [0] * (l-len(p[i])))
        else:
            # Completion with zeros to get the wanted degree
            P_extended.append([0] * l)
    return P_extended


def convert_to_int(P, q):
    """Converts each coefficients of the polynomial P into an integer modulo q (i.e. an element of Zq)

    Args:
        P (matrix): bivariate polyniomal
        q (integer): modulo (i.e. order of the group)

    Returns:
        matrix: bivariate polynomial with all its coefficients being an element of Zq
    """
    result = []
    for i in range(len(P)):
        result.append([])
        for j in range (len(P[i])):
            result[i].append(int(round(P[i][j]) % q))
    return result


def division(P, Q, q):
    """Divides the bivariate polynomial P by the bivariate polynomial Q

    Args:
        P (matrix): bivariate polynomial (dividend)
        Q (matrix): bivariate polynomial (divisor)
        q (int): order of the group

    Returns:
        matrix: bivariate polynomial (quotient of P divided by Q)
    """

    # Computation of degrees in X and in Y of P, Q and the quotient of P divided by Q
    dP = len(P)
    lP = len(P[0])
    dQ = len(Q)
    lQ = len(Q[0])
    dR = dP - dQ + 1
    lR = lP - lQ + 1

    # We extend Q to have to same size than P
    Q_extended = extend_polynomial(Q, dP, lP)

    # FFT of P and Q
    Pfft = fft2(P)
    Qfft = fft2(Q_extended)

    # Inversed FFT of division of P by Q 
    P_divided_by_Q = list(ifft2(Pfft/Qfft).real)

    # Each coefficient is converted into element of Zq
    result = convert_to_int(P_divided_by_Q, q)

    # Reduction of the polynomial size to have the required degrees in X and in Y
    reducted_result = []
    for i in range (dR):
        reducted_result.append([])
        for j in range (lR):
            reducted_result[i].append(result[i][j])
    
    return reducted_result




def multiplication(P, Q, q):
    """Multiplication of P by Q

    Args:
        P (matrix): bivariate polynomial (first factor of the multiplication)
        Q (matrix): bivariate polynomial (second factor of the multiplication)
        q (int): order of the group

    Returns:
        matrix: bivariate prolynomial (product of P multiplied by Q)
    """
    # Computation of degrees in X and in Y of P, Q and the product of P multiplied by Q
    dP = len(P)
    lP = len(P[0])
    dQ = len(Q)
    lQ = len(Q[0])
    dR = dP + dQ - 1
    lR = lP + lQ - 1

    # We extend P and Q to have to same size than P+Q
    P_extended = extend_polynomial(P, dR, lR)
    Q_extended = extend_polynomial(Q, dR, lR)

    # FFT of P and Q
    Pfft = fft2(P_extended)
    Qfft = fft2(Q_extended)
    
    # Inversed FFT of Multiplication of P by Q 
    P_times_Q = list(ifft2(Pfft*Qfft).real)
    
    # Each coefficient is converted into element of Zq
    result = convert_to_int(P_times_Q, q)

    return result