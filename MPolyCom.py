from BivPolyCom import Biv

class MPoly:

    def __init__(self, group, d, l):
        self.group = group
        self.d = d
        self.l = l

    def ComGen(self, d, l):
        """Generates the commitment key
        Args:
            d (int): degree in X
            l (int): degree in Y

        Returns:
            dictionary: commitment key
        """
        ck = Biv.ComGen(self, d, l)
        return ck


    def Com(self, ck, P):
        """Creates of the commitment for the list P of univariate polynomials

        Args:
            ck (dictionary): commitment key
            P (matrix): committed list of univariate polynomials

        Returns:
            dictionary: two uniformly distributed elements of G1²
            ZR element: random ZR element
        """
        C, rho = Biv.Com(self, ck, P)
        return C, rho

    def ComVer(self, ck, C):
        """Verifies the commitment

        Args:
            ck (dictionary): commitment key
            C (dictionary): values created from the committed list of polynomals

        Returns:
            boolean: verification of the commitment
        """
        b = Biv.ComVer(self, ck, C)
        return b

    def OpenVer(self, ck, C, P, rho):
        """Global verification

        Args:
            ck (dictionary): commitment key
            C (dictionary): values computed from the committed polynomial
            a (matrix): committed bivariate polynomial
            rho (ZR element): random value used to compute C

        Returns:
            boolean: validation
        """
        b1andb2 = Biv.OpenVer(self, ck, C, P, rho)
        return (b1andb2)