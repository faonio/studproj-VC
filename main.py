import random as rd
from charm.toolbox.pairinggroup import PairingGroup
from BivPolyCom import Biv
from CaP_SNARK_BivPE import BivPE
from MPolyCom import MPoly
from CaP_SNARK_MultiUniEv import MultiUniPE

group = PairingGroup('SS512')


###########################################
# BivPolyCom Test
###########################################

# Modify here d and l variables
d = 10
l = 10

# This is the bivariate polynomial P
P = []
for i in range (d):
    P.append([])
    for j in range(l):
        P[i].append(rd.randrange(0,20))

biv_example = Biv(group, d, l)
ck = biv_example.ComGen(d, l)
C, rho = biv_example.Com(ck, P)
open_ver = biv_example.OpenVer(ck, C, P, rho)

print("Verification of Bivariate Polynomial Commitment = ", open_ver)



###########################################
# CaP-SNARK for BivPE Test
###########################################

# Modify here the value of k
k = rd.randrange(0, 10)

#This is the bivariate polynomial Q(Y) = P(k, Y)
Q1 = []
for j in range (l):
    sum = 0
    for i in range(d):
        sum = sum + P[i][j]*(k**i)
    Q1.append(sum)
Q = [Q1]
for i in range (1, d):
    Q.append([])
    for j in range(l):
        Q[i].append(0)

bivPE_ex = BivPE(group, biv_example, d, l)
crs, u, w = bivPE_ex.Gen(ck, P, Q, k)
pi = bivPE_ex.Prove(crs, u, w)
verif = bivPE_ex.Ver(crs, u, pi)

print("Verification of CaP-SNARK for BivPE = ", verif)


##################################################
# Multiple Univariate Polynomials Commitment Test
##################################################

#Modify here d and l variables
d = 10
l = 10

#This is the list of univariate polynomials Pj
P = []
for j in range (l):
    P.append([])
    for i in range(d):
        P[j].append(rd.randrange(0,20))

mul_uni_example = MPoly(group, d, l)
ck = mul_uni_example.ComGen(d, l)
C, rho = mul_uni_example.Com(ck, P)
open_ver = mul_uni_example.OpenVer(ck, C, P, rho)

print("Verification of Multiple Univariate Polynomials Committment = ", open_ver)


###########################################
# CAP-SNARK for MultiUniEv Test
###########################################

# Modify here the value of k
k = rd.randrange(0, 10)

#This is the univariate polynomial Q(Y) = P(k, Y)
Q1 = []
for j in range (l):
    sum = 0
    for i in range(d):
        sum = sum + P[i][j]*(k**i)
    Q1.append(sum)
Q = [Q1]
for i in range (1, d):
    Q.append([])
    for j in range(l):
        Q[i].append(0)

multiUniPE_ex = MultiUniPE(group, mul_uni_example, d, l)
crs, u, w = multiUniPE_ex.Gen(ck, P, Q, k)
pi = multiUniPE_ex.Prove(crs, u, w)
verif = multiUniPE_ex.Ver(crs, u, pi)

print("Verification of CaP-SNARK for MultiUniEv = ", verif)


