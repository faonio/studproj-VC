# Commit-and-Prove SNARK

The only file you have to modify to use the code is the file main.py.

### Test of Bivariate Polynomial Commitment (BivPolyCom.py)

In this part, you can modify:
- the value of d and l which are the degrees in X and Y of the bivariate polynomial P (*if the values are too big, we face some issues that are currently unsolved*)
- the biariate polynomial P

Then, you can:
- create a new instance of the class Biv
- generate the commitment key ck
- use that key to creates the commitment for the bivariate polynomial P
- verify that commitment

The current printed boolean is the global verification. If the commitment is well created, the value is 'True'.


### Test of CaP-SNARK for Bivariate Polynomial Evaluation (CaP_SNARK_BivPE.py)

To test the CaP-SNARK for Bivariate Polynomial Evaluation, we are going to re-use the commitment created previously.

In this part, you can modify:
- the value of k such that the bivariate polynomial P will be partially evaluate in k (for the X variable)

Then, you can:
- create the polynomial Q = P(k,Y)
- create a new instance of BivPE
- generate crs which contains all the needed variable for the CaP, u the statement and w the witness
- use that crs, u and w to generate a proof that the CaP relation is satisfied
- verify the proof

The current printed boolean is the verification of the proof. If the proof is verified, i.e if the CaP relation is satisfied, the value is 'True'.


### Test of Multiple Univariate Polynomials Commitment (MPolyCom.py)

In this part, you can modify:
- the value of d which is the degree in X of the each univariate polynomial Pj
- the value of l which is the number of univariate polynomials 
- all the univariate polynomials Pj
(*if the values of d and l are too big, we face some issues that are currently unsolved*)

Then, you can:
- create a new instance of the class MPoly
- generate the commitment key ck
- use that key to creates the commitment for the list P of all the univariate polynomials
- verify that commitment

The current printed boolean is the global verification. If the commitment is well created, the value is 'True'.


### Test of CaP-SNARK for Multiple Univariate Polynomials Evaluation(CaP_SNARK_MultiUniEv.py)

To test the CaP-SNARK for Multiple Univariate Polynomial Evaluation, we are going to re-use the commitment created previously.

In this part, you can modify:
- the value of k such that all the univariate polynomials Pj will be evaluate in k

Then, you can:
- create the univariate polynomial Q = P(k,Y)
- create a new instance of MultiUniPE
- generate crs which contains all the needed variable for the CaP, u the statement and w the witness
- use that crs, u and w to generate a proof that the CaP relation is satisfied
- verify the proof

The current printed boolean is the verification of the proof. If the proof is verified, i.e if the CaP relation is satisfied, the value is 'True'.